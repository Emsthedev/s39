const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseController');
const auth = require('../auth');


//Route for creating a course
// router.post('/', (req,res)=>{
//     courseController.addCourse(req.body)
//     .then(resultFromController => 
//         res.send(resultFromController))
// });

router.post('/', auth.verify, (req,res)=>{
    const courseData = auth.decode(req.headers.authorization)
    console.log(courseData)

    if(!courseData.isAdmin){
        return res.send('You are not the Admin')
    }
    courseController.addCourse(req.body)
    .then(resultFromController => 
        res.send(resultFromController))
});



// router.post("/", auth.verify, (req, res)=>{
    
//     const courseData = auth.decode(req.headers.authorization)
//     console.log(courseData)

//     courseController.verifyIsAdmin({isAdmin: courseData.isAdmin})
//     .then(resultFromController => 
//         res.send(resultFromController))
// })


module.exports = router