const Course = require('../models/Course');


//addCourse function
/* module.exports.addCourse = (reqBody) => {
    //Created a variable "newCourse" and instantiate a new "Course" object
    //using the mongoose model.
    //This uses the information from the request body to provide 
    //all the neccessary information
    let newCourse = new Course({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    });
        // Saves the created object to our DB using .save()
    return newCourse.save().then((course, error)=> {
        //course creation failed
        if(error){
            return false
        }
        //Course creation successful
        else{
            return true
        }
    })
};
 */
module.exports.addCourse = (reqBody) => {
    //Created a variable "newCourse" and instantiate a new "Course" object
    //using the mongoose model.
    //This uses the information from the request body to provide 
    //all the neccessary information
    let newCourse = new Course({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    });
        // Saves the created object to our DB using .save()


    return newCourse.save().then((course, error)=> {
        //course creation failed
   
        if(error){
            return false
        }
        //Course creation successful
        else{
            return true
        }
    })
}






// module.exports.verifyIsAdmin = (courseData) => {
//     return Course.find(courseData.isAdmin)
//     .then(result => {
//         console.log(courseData)
//         if(result == false) {
//           return false
//         }else {
//         console.log(result)
//         return true
//         }
//     })
// };